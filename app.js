var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

// var indexRouter = require('./routes/index');
// var usersRouter = require('./routes/users');

/*Jwt token verifications*/
// var User = require('./models/users');
jsonwebtoken = require("jsonwebtoken");

var app = express();
// view engine setup

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/*Connect database*/
var Mongoose = require('mongoose');
Mongoose.Promise = global.Promise;

/****MongoDB credentials*******************/
// var mongoLogin = MongoConfig.username;
// var mongoPass = MongoConfig.pass;

var url = "mongodb://localhost:27017/vrobeauty";
/******************************************/

const MongoConnectOptions = {
	useMongoClient: true,
	promiseLibrary: global.Promise,keepAlive: true,
  // reconnectTries: 30,
  // reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  // reconnectInterval: 500, // Reconnect every 500ms
};
// var mongoURI = String(MongoConfig.uri).replace("{username}", mongoLogin).replace("{password}", mongoPass).replace("{dbname}", ConfigMain.getDatabaseName());
console.log("Connecting to database...");
Mongoose.connect(url, MongoConnectOptions);
var db = Mongoose.connection;
db.on('error', function(err) {
	console.log("Failed to connect to Database: " + err);
});
db.once('open', function() {
	console.log("Database connection successfull");  
});
// Add headers
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.use('/api/v1', require('./routes/users'));
app.use('/api/v1', require('./routes/posts'));
app.use('/api/v1', require('./routes/competitions'));
app.use('/api/v1', require('./routes/seasons'));
app.use('/api/v1', require('./routes/favourits'));
app.use('/api/v1', require('./routes/follows'));
app.use('/api/v1', require('./routes/votes'));
app.use('/api/v1', require('./routes/winners'));
app.use('/api/v1', require('./routes/externals'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// app.use(function(req, res, next) {
// 	if(req.headers && req.headers.authorization && req.headers.authorization.split('')[0] == 'JWT')
// 	{
// 		jsonwebtoken.verify(req.headers.authorization.split('')[1],'RESTFULAPIs',function(err,decode){
// 			if(err) req.user = undefined;
// 			req.user = decode;
// 			next();
// 		});
// 	}else{
// 		req.user = undefined;
// 		next();
// 	}
// });


module.exports = app;
