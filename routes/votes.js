var express = require('express');
const VoteController = require('../controllers/voteController');

var router = express.Router();

router.post('/votes/',VoteController.create);
router.get('/votes/',VoteController.lists);
router.delete('/votes/:id',VoteController.delete);

module.exports = router;


