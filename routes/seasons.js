var express = require('express');
const SeasonController = require('../controllers/seasonController');

var router = express.Router();

router.post('/seasons/',SeasonController.create);
router.get('/seasons/',SeasonController.lists);
router.get('/seasons/:id',SeasonController.show);
router.put('/seasons/:id',SeasonController.update);
router.delete('/seasons/:id',SeasonController.delete);

module.exports = router;


