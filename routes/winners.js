var express = require('express');
const WinnerController = require('../controllers/winnerController');

var router = express.Router();

router.post('/winners/',WinnerController.create);
router.get('/winners/',WinnerController.lists);
router.get('/winners/:id',WinnerController.show);
router.put('/winners/:id',WinnerController.update);
router.delete('/winners/:id',WinnerController.delete);

module.exports = router;


