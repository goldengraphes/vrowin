var express = require('express');
const UserController = require('../controllers/userController');

var router = express.Router();

// router.get('/users/',UserController.listAll);
router.post('/users/',UserController.store);
router.get('/getprofile/',UserController.getprofile);
// router.get('/users/:id',UserController.show);
// router.put('/users/:id',UserController.update);
// router.delete('/users/:id',UserController.delete);

/*user authentications*/
// router.post('/login',UserController.login);

module.exports = router;


