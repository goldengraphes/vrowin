var express = require('express');
const UserController = require('../controllers/competitionController');

var router = express.Router();

router.post('/competitions/',UserController.create);
router.get('/competitions/',UserController.lists);
router.get('/competitions/:id',UserController.show);
router.put('/competitions/:id',UserController.update);
router.delete('/competitions/:id',UserController.delete);

module.exports = router;


