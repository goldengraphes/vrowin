var express = require('express');
const FollowController = require('../controllers/followController');

var router = express.Router();

router.post('/follows/',FollowController.create);
router.get('/follows/',FollowController.lists);
router.delete('/follows/:id',FollowController.delete);

module.exports = router;


