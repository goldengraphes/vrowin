var express = require('express');
const ExternalController = require('../controllers/externalController');
var router = express.Router();

router.get('/startCompetions/',ExternalController.startCompetions);
router.get('/getCompetitions/',ExternalController.getCompetitions);
router.get('/getSeasonRounds/',ExternalController.getSeasonRounds);
router.get('/getRoundDetail/',ExternalController.getRoundDetail);

module.exports = router;

