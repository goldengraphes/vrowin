var express = require('express');
const UserController = require('../controllers/postController');

var router = express.Router();

router.post('/posts/',UserController.create);
router.get('/posts/',UserController.lists);
router.get('/posts/:id',UserController.show);
router.delete('/posts/:id',UserController.delete);

module.exports = router;


