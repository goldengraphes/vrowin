var express = require('express');
const FavouritController = require('../controllers/favouritsController');

var router = express.Router();

router.post('/favourits/',FavouritController.create);
router.get('/favourits/',FavouritController.lists);
router.delete('/favourits/:id',FavouritController.delete);

module.exports = router;


