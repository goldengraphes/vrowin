/*Beauty Post model*/
const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

const winnerSchema = new Schema({
	user_id: {
		type: String,
		required: true
	},
	post_id: {
		type: String,
		required: true
	},
	season_id: {
		type: String,
		required: true
	},
	competition_id: {
		type: String,
		required: true
	},
}, {
	usePushEach: true
});

module.exports = Mongoose.model('WinnerBeauty', winnerSchema);
