const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;
const SeasonTalent = require('../seasons/seasonTalent');

const competitionSchema = new Schema({
	competitionName: {
		type: String,
		required: true
	},
	status: {
		type: Number,
		default:1 
	},
	active_season: {
		type: Mongoose.Schema.Types.ObjectId,
		ref: 'SeasonBeauty'
	},
	archived_season: [{
		type: Mongoose.Schema.Types.ObjectId,
		ref: 'SeasonTalent'
	}],
	waiting_season: [{
		type: Mongoose.Schema.Types.ObjectId,
		ref: 'SeasonBeauty'
	}],
	created_at: {
		type: Date,
		default: Date.now
	}
}, {
	usePushEach: true
});

module.exports = Mongoose.model('CompetitionTalent', competitionSchema);
