/*Beauty Post model*/
const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;
const RoundBeauty = require('../rounds/roundBeauty');

const SeasonSchema = new Schema({
	seasonName: {
		type: String,
		required: true
	},
	status: {
		type: Number,
		default:0     // 0-pending 1-active 2-archieved
	},
	current_rounds: {
		type: Number,
		default:0
	},
	daysIntervel: {
		type: Number,
		default:0 
	},
	winner_id: {
		type:String,
		default:''
	},
	startOn: {
		type: Date,
		default:''
	}, 
	city: {
		type: String,
		default:''
	},
	state: {
		type: String,
		default:''
	},
	country: {
		type: String,
		default:''
	},
	active_round: {
		type: Mongoose.Schema.Types.ObjectId,
		ref: 'RoundBeauty'
	},
	archived_rounds:[{
		type: Mongoose.Schema.Types.ObjectId,
		ref: 'RoundBeauty'
	}],
	created_at: {
		type: Date,
		default: Date.now
	}
}, {
	usePushEach: true
});

module.exports = Mongoose.model('SeasonBeauty', SeasonSchema);
