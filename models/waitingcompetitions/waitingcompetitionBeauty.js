const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;
const CompetitionBeauty = require('../competitions/competitionBeauty');

const competitionSchema = new Schema({
	competition: {
		type: Mongoose.Schema.Types.ObjectId,
		ref: 'CompetitionBeauty'
	}
}, {
	usePushEach: true
});

module.exports = Mongoose.model('WaitingCompetitionBeauty', competitionSchema);
