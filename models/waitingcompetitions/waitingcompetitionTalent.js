const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;
const CompetitionTalent = require('../competitions/competitionTalent');

const competitionSchema = new Schema({
	competition: {
		type: Mongoose.Schema.Types.ObjectId,
		ref: 'CompetitionTalent'
	}
}, {
	usePushEach: true
});

module.exports = Mongoose.model('WaitingCompetitionTalent', competitionSchema);
