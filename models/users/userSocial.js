const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

const UserSchema = new Schema({
	name: {
		type: String,
		default:''
	},
	email: { 
		type: String, 
		unique: true, 
		required: true
	},
	phone: {
		type: String,
		default:'' 
	},
	userToken: { 
		type: String,
		default:''
	},
	language: {
		type: String,
		default:'EN'
	},
	profile_image:{
		type: String,
		default:''
	},
	fans:{
		type: Number,
		default:0
	},
	follows:{
		type: Number,
		default:0
	},
	status: {
		type: Number,
		default:1   //1-active and 0-inactive
	},
	device_token: {
		type: String,
		default:''
	},
	description: {
		type: String,
		default:''
	},
	city: {
		type: String,
		default:''
	},
	state: {
		type: String,
		default:''
	},
	country: {
		type: String,
		default:''
	},
	location: {
		type: {type: String,default: 'Point' },
		coordinates: []
	},
	updated_at: {
		type: Date,
		default: Date.now
	},
	created_at: {
		type: Date,
		default: Date.now
	}
}, {
	usePushEach: true
});


module.exports = Mongoose.model('UserSocial', UserSchema);
