/*Beauty Post model*/
const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;
const UserSocial = require('../users/userSocial');

const followSchema = new Schema({
	user_id: {
		type: String,
		required: true
	},
	follow_id: {
		type: Mongoose.Schema.Types.ObjectId,
		ref: 'UserSocial'
	}
}, {
	usePushEach: true
});

module.exports = Mongoose.model('FollowSocial', followSchema);
