/*Tanlent Post model*/
const UserTalent = require('../users/userTalent');
const SeasonTalent = require('../seasons/seasonTalent');
const RoundTalent = require('../users/userTalent');

const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

const postSchema = new Schema({
	user_id: {
		type: Mongoose.Schema.Types.ObjectId,
		ref: 'UserTalent'
	},
	post_url: {
		type: String,
		default:'' 
	},
	post_image: {																																																																								
		type: String,
		default:'' 
	},
	post_type: {
		type: String,
		default:'0'    //0-image 1-video 
	},
	isCompetition: {
		type: String, 
		default:0 
	},
	competition_id:{
		type: String,
		default:''
	},
	season_id: { 
		type: Mongoose.Schema.Types.ObjectId,
		ref: 'SeasonTalent'
	},
	round_id: { 
		type: Mongoose.Schema.Types.ObjectId,
		ref: 'RoundTalent'
	},
	hashtags: { 
		type: Array,
		default:[]
	},
	description: { 
		type: String,
		default:''
	},
	votes: { 
		type: Number,
		default:0
	},
	favourits: { 
		type: Number,
		default:0
	},
	created_at: {
		type: Date,
		default: Date.now
	}	
}, {
	usePushEach: true
});

module.exports = Mongoose.model('PostTalent', postSchema);
