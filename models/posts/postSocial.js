/*Social Post model*/
const UserSocial = require('../users/userSocial');

const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

const postSchema = new Schema({
	user_id: {
		type: Mongoose.Schema.Types.ObjectId,
		ref: 'UserSocial'
	},
	post_url: {
		type: String,
		default:'' 
	},
	post_image: {
		type: String,
		default:'' 
	},
	post_type: {
		type: String,
		default:'0'    //0-image 1-video 
	},
	hashtags: { 
		type: Array,
		default:[]
	},
	votes: { 
		type: Number,
		default:0
	},
	favourits: { 
		type: Number,
		default:0
	},
	updated_at: {
		type: Date,
		default: Date.now
	},
	created_at: {
		type: Date,
		default: Date.now
	}
}, {
	usePushEach: true
});

module.exports = Mongoose.model('PostSocial', postSchema);
