/*Beauty Post model*/
const UserBeauty = require('../users/userBeauty');
const RoundBeauty = require('../users/userBeauty');
const SeasonBeauty = require('../seasons/seasonBeauty');

const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

const postSchema = new Schema({
	user_id: {
		type: Mongoose.Schema.Types.ObjectId,
		ref: 'UserBeauty'
	},
	post_url: {
		type: String,
		default:'' 
	},
	post_image: {																																																																								
		type: String,
		default:'' 
	},
	post_type: {
		type: String,
		default:'0'    //0-image 1-video 
	},
	isCompetition: {
		type: String, 
		default:0 
	},
	competition_id:{
		type: String,
		default:''
	},
	season_id: { 
		type: Mongoose.Schema.Types.ObjectId,
		ref: 'SeasonBeauty'
	},
	round_id: { 
		type: Mongoose.Schema.Types.ObjectId,
		ref: 'RoundBeauty'
	},
	hashtags: { 
		type: Array,
		default:[]
	},
	description: { 
		type: String,
		default:''
	},
	votes: { 
		type: Number,
		default:0
	},
	favourits: { 
		type: Number,
		default:0
	},
	created_at: {
		type: Date,
		default: Date.now
	}	
}, {
	usePushEach: true
});

module.exports = Mongoose.model('PostBeauty', postSchema);
