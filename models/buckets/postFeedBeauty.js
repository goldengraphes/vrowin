/*Beauty Post model*/
const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;
const PostBeauty = require('../posts/postBeauty');

const postFeedSchema = new Schema({
	post_id: [{
		type: Mongoose.Schema.Types.ObjectId,
		ref: PostBeauty
	}],
	seq: {
		type: Number,
		default:0
	}
}, {
	usePushEach: true
});

module.exports = Mongoose.model('postFeedBeauty', postFeedSchema);


