/*Beauty Post model*/
const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;
const PostTalent = require('../posts/postTalent');


const postFeedSchema = new Schema({
	post_id: [{
		type: Mongoose.Schema.Types.ObjectId,
		ref: PostTalent
	}],
	seq: {
		type: Number,
		default:0
	}
}, {
	usePushEach: true
});

module.exports = Mongoose.model('postFeedTalent', postFeedSchema);
