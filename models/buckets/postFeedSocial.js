/*Beauty Post model*/
const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;
const PostSocial = require('../posts/postSocial');

const postFeedSchema = new Schema({
	post_id: [{
		type: Mongoose.Schema.Types.ObjectId,
		ref: PostSocial
	}],
	seq: {
		type: Number,
		default:0
	}
}, {
	usePushEach: true
});
module.exports = Mongoose.model('postFeedSocial', postFeedSchema);
