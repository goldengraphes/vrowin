/*Beauty Post model*/
const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;
const PostSocial = require('../posts/postSocial');

const favouritSchema = new Schema({
	user_id: {
		type: String,
		required: true
	},
	post_id: [{
		type: Mongoose.Schema.Types.ObjectId,
		ref: 'PostSocial'
	}]
}, {
	usePushEach: true
});

module.exports = Mongoose.model('FavouritSocial', favouritSchema);
