/*Beauty Post model*/
const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;
const PostBeauty = require('../posts/postBeauty');

const favouritSchema = new Schema({
	user_id: {
		type: String,
		required: true
	},
	post_id: [{
		type: Schema.Types.ObjectId,
		ref: 'PostBeauty'
	}]
}, {
	usePushEach: true
});

module.exports = Mongoose.model('FavouritBeauty', favouritSchema);
