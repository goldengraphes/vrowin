/*Beauty Post model*/
const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;
const PostBeauty = require('../posts/postBeauty');

const VoteSchema = new Schema({
	user_id: {
		type: String,
		required: true
	},
	post_id: [{
		type: Mongoose.Schema.Types.ObjectId,
		ref: 'PostBeauty'
	}]
}, {
	usePushEach: true
});

module.exports = Mongoose.model('VoteBeauty', VoteSchema);

