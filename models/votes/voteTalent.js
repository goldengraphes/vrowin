/*Beauty Post model*/
const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;
const PostTalent = require('../posts/postTalent');

const voteSchema = new Schema({
	user_id: {
		type: String,
		required: true
	},
	post_id: [{
		type: Mongoose.Schema.Types.ObjectId,
		ref: 'PostTalent'
	}]

}, {
	usePushEach: true
});

module.exports = Mongoose.model('VoteTalent', voteSchema);
