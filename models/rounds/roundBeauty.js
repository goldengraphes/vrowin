/*Beauty Post model*/
const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;
const PostBeauty = require('../posts/postBeauty');
const UserBeauty = require('../users/userBeauty');


const SeasonSchema = new Schema({	
	posts:[{
		type: Mongoose.Schema.Types.ObjectId,
		ref: 'PostBeauty'
	}],
	winnerposts:[{
		type: Mongoose.Schema.Types.ObjectId,
		ref: 'PostBeauty'
	}],
	winnerusers:[{
		type: Mongoose.Schema.Types.ObjectId,
		ref: 'UserBeauty'
	}],
	rounds:{
		type:Number,
		default:1
	}
}, {
	usePushEach: true
});

module.exports = Mongoose.model('RoundBeauty', SeasonSchema);
