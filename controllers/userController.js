var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var UserBeauty = require('../models/users/userBeauty');
var UserTalent = require('../models/users/userTalent');
var UserSocial = require('../models/users/userSocial');
/*User Register*/
var FavouritBeauty = require('../models/favourits/favouritBeauty');
var FavouritTalent = require('../models/favourits/favouritTalent');
var FavouritSocial = require('../models/favourits/favouritSocial');


exports.store = (req, res) => {

	var user = new UserBeauty(req.body);
	user.save((err, user) => {
		if (err) {
			res.status(400).send({code:400,message:err.errors})
		} else {
			res.status(200).send({code:200,message:'User Registered successfully',data:user})
		}

	});
}

/* Get profile data */
exports.getprofile = function(req, res) {
	var users = new Promise((resolve, reject) => { 
		UserBeauty.findOne({
			_id: req.query.user_id
		}, function(err, user) {
			resolve(user);
		});
	}); 
	var likes = new Promise((resolve, reject) => { 
		FavouritBeauty.findOne({
			user_id: req.query.user_id
		}, function(err, count) {
			resolve(count);
		}).count('post_id');
	});
	var favouritslist = new Promise((resolve, reject) => {
		FavouritBeauty.findOne({user_id:req.query.user_id}, function(err, favorites) {
			resolve(favorites)
		}).populate('post_id');	
	});
	Promise.all([users, likes, favouritslist])
	.then(values => { 
		return res.status(200).send({code:200,data:{
			user:values[0],
			likes:values[1],
			favouritlist:values[2],
		}
	})
	})
	.catch(error => { 
		console.log(error.message)
	});
};



/*user login with authentications*/
// exports.login = (req, res) => {
//   User.findOne({
//     email: req.body.email
//   }, function(err, user) {
//     if (err) return res.send({error:err});
//     if (!user) {
//       res.status(401).send({code:401, message: 'Authentication failed. User not found.' });
//     } else if (user) {
//       if (!user.comparePassword(req.body.password)) {
//         res.status(401).send({code:401, message: 'Authentication failed. Wrong password.' });
//       } else {
//         return res.send({code:200,token: jwt.sign({ email: user.email, userName: user.userName, _id: user._id}, 'RESTFULAPIs'), data:user});
//       }
//     }
//   });
// };

/*Check for islogin authentications*/
// exports.islogin = function(req, res, next) {
//   if (req.user) {
//     next();
//   } else {
//     return res.status(401).send({ code:401, message: 'Unauthorized Request!' });
//   }
// };

/*list all users*/
// exports.listAll = function(req, res) {
//   User.find({}, function(err, users) {
//     if (err) return res.status(401).send({code:401,error:err});
//     return res.status(200).send({code:200,data:users})
//   });
// };

/*get single user data detail*/
// exports.show = function(req, res) {
//   User.findOne({
//     _id: req.params.id
//   }, function(err, user) {
//     if (err) return res.status(401).send({code:401,error:err});
//     return res.status(200).send({code:200,data:user});
//   });
// };

// exports.update = function(req, res) {
//   User.findByIdAndUpdate({
//     _id: req.params.id
//   },req.body, function(err, user) {
//     if (err) return res.status(401).send({code:401,error:err});
//     return res.status(200).send({code:200,message:"Successfully updated"});
//   });
// };

// exports.delete = function(req, res, next) {
//   User.findOneAndDelete({
//     _id: req.params.id
//   },req.body, function(err, user) {
//     if (err) return res.status(401).send({code:401,error:err});
//     return res.status(200).send({code:200,message:"Deleted successfully"});
//   });
// };

