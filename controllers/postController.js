
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var PostBeauty = require('../models/posts/postBeauty');
var PostTalent = require('../models/posts/postTalent');
var PostSocial = require('../models/posts/postSocial');
var PostFeedBeauty = require('../models/buckets/postFeedBeauty');
var PostFeedTalent = require('../models/buckets/postFeedTalent');
var PostFeedSocial = require('../models/buckets/postFeedSocial');
var RoundBeauty = require('../models/rounds/roundBeauty');
var RoundTalent = require('../models/rounds/roundTalent');

exports.create = (req, res) => {
	var post='';;
	if(req.headers.type == "vrobeauty")
		post = new PostBeauty(req.body);
	else if(req.headers.type == "vrotalent")		
		post = new PostTalent(req.body);
	else if(req.headers.type == "vrosocial")
		post = new PostSocial(req.body);
	else
		return res.status(400).send({code:400,message:"Type is not acceptable"})
	post.save((err, feed) => {
		if (err) {
			return res.status(400).send({code:400,message:err.errors})
		} else {
			//update in rounds
			updateRound(feed.round_id,feed._id,req.headers.type)

			if(req.headers.type == "vrobeauty"){
				PostFeedBeauty.findOne().sort({'_id':-1}).exec((err, buckets)=>{
					if (buckets.post_id && buckets.post_id.length < 20)
					{
						updateBucketPostFeed(buckets._id,feed._id,req.headers.type);
					}else{
						createBucketPostFeed(feed._id,buckets.seq+1,req.headers.type);
					}
				});
			}else if(req.headers.type == "vrotalent"){
				PostFeedTalent.findOne().sort({'_id':-1}).exec((err, buckets)=>{
					if (buckets.post_id && buckets.post_id.length < 20)
					{
						updateBucketPostFeed(buckets._id,feed._id,req.headers.type);
					}else{
						createBucketPostFeed(feed._id,buckets.seq+1,req.headers.type);
					}
				});
			}else{
				PostFeedSocial.findOne().sort({'_id':-1}).exec((err, buckets)=>{
					if (buckets.post_id && buckets.post_id.length < 20)
					{
						updateBucketPostFeed(buckets._id,feed._id,req.headers.type);
					}else{
						createBucketPostFeed(feed._id,buckets.seq+1,req.headers.type);
					}
				});
			}
		}
		return res.status(200).send({code:200,message:'Posted successfully',data:feed})
	});
}


exports.lists = (req, res) => {
	var bucketsNo = parseInt(req.query.bucket);
	if(bucketsNo == 0){
		switch(req.headers.type){
			case "vrobeauty":
			PostFeedBeauty.findOne().sort({'_id':-1}).populate('post_id').exec((err, buckets)=>{
				return res.status(200).send({code:200,data:buckets});
			});
			break;
			case "vrotalent":
			PostFeedTalent.findOne().sort({'_id':-1}).populate('post_id').exec((err, buckets)=>{
				return res.status(200).send({code:200,data:buckets})
			});
			break;
			case "vrosocial":
			PostFeedSocial.findOne().sort({'_id':-1}).populate('post_id').exec((err, buckets)=>{
				return res.status(200).send({code:200,data:buckets})
			});
			break;
			default: return res.status(200).send({code:200,data:[]})
		}
	}else{
		switch(req.headers.type){
			case "vrobeauty":
			PostFeedBeauty.findOne({'seq':bucketsNo}, function(err, buckets) {
				return res.status(200).send({code:200,data:buckets})
			}).populate('post_id');	
			break;
			case "vrotalent":
			PostFeedTalent.findOne({'seq':bucketsNo}, function(err, buckets) {
				return res.status(200).send({code:200,data:buckets})
			}).populate('post_id');	
			break;
			case "vrosocial":
			PostFeedSocial.findOne({'seq':bucketsNo}, function(err, buckets) {
				return res.status(200).send({code:200,data:buckets})
			}).populate('post_id');	
			break;
			default: return res.status(200).send({code:200,data:[]})
		}
	}
}

exports.show = function(req, res) {
	switch(req.headers.type ){
		case "vrobeauty":
		PostBeauty.findOne({
			_id: req.params.id
		}, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,data:user});
		});
		break;
		case "vrotalent":
		PostTalent.findOne({
			_id: req.params.id
		}, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,data:user});
		});
		break;
		case "vrosocial":
		PostSocial.findOne({
			_id: req.params.id
		}, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,data:user});
		});
		break;
		default: return res.status(400).send({code:400,message:"Type is not acceptable"})
	}
};

exports.delete = function(req, res, next) {
	switch(req.headers.type ){
		case "vrobeauty":
		PostBeauty.findOneAndDelete({
			_id: req.params.id
		},req.body, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,message:"Deleted successfully"});
		});
		break;
		case "vrotalent":
		PostTalent.findOneAndDelete({
			_id: req.params.id
		},req.body, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,message:"Deleted successfully"});
		});
		break;
		case "vrosocial":
		PostSocial.findOneAndDelete({
			_id: req.params.id
		},req.body, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,message:"Deleted successfully"});
		});
		break;
		default: return res.status(400).send({code:400,message:"Type is not acceptable"})
	}
};

exports.updatePost = function(fav_id,post_id,type){
	switch(type ){
		case "vrobeauty":
		PostBeauty.findByIdAndUpdate({
			_id: post_id
		},{ "$push": { "favourites": fav_id }}, function(err, user) {
			if (err) return err;
			return 1;
		});
		break;
		case "vrotalent":
		PostTalent.findByIdAndUpdate({
			_id: post_id
		},{ "$push": { "favourites": fav_id }}, function(err, user) {
			if (err) return err;
			return 1;
		});
		break;
		default: PostSocial.findByIdAndUpdate({
			_id: post_id
		},{ "$push": { "favourites": fav_id }}, function(err, user) {
			if (err) return err;
			return 1;
		});
		break;
	}
};

function updateBucketPostFeed(bucketid,postid,type){
	switch(type){
		case "vrobeauty": PostFeedBeauty.findByIdAndUpdate({
			_id: bucketid
		},{ "$push": { "post_id": postid }}, function(err, user) {
			if (err) console.log(err);
		});
		break;
		case "vrotalent": PostFeedTalent.findByIdAndUpdate({
			_id: bucketid
		},{ "$push": { "post_id": postid }}, function(err, user) {
			if (err) console.log(err)
		});
		break;
		case "vrotalent": PostFeedSocial.findByIdAndUpdate({
			_id: bucketid
		},{ "$push": { "post_id": postid }}, function(err, user) {
			if (err) console.log(err)
		});
		break;
		default: console.log("failed on type");
		break;
	}
};

function createBucketPostFeed(postid,inc,type){
	switch(type){
		case "vrobeauty":
		bucketpostbeauty = new PostFeedBeauty({"post_id":[postid],"seq":inc});
		bucketpostbeauty.save((err, user) => {
			if(err) console.log(err); 
			if(user) console.log("success",user); 
		});
		break;
		case "vrotalent":
		bucketposttalent = new PostFeedTalent({"post_id":[postid],"seq":inc});
		bucketpostbeauty.save((err, user) => {
			if(err) console.log(err); 
			if(user) console.log("success"); 
		});
		break;
		case "vrosocial":
		bucketpostsocial = new PostFeedSocial({"post_id":[postid],"seq":inc});
		bucketpostbeauty.save((err, user) => {
			if(err) console.log(err); 
			if(user) console.log("success"); 
		});
		break;
		default: console.log("failed on type");
		break;
	}
};

function updateRound(roundid,postid,type){
	switch(type){
		case "vrobeauty":
		RoundBeauty.findByIdAndUpdate({_id: roundid},{ "$push": { "posts": postid }}, function(err, user) {
		});
		break;
		case "vrotalent":
		RoundTalent.findByIdAndUpdate({_id: roundid},{ "$push": { "posts": postid }}, function(err, user) {
		});
		break;
		break;
		default: console.log("failed on type");
		break;
	}
};


