var FollowBeauty = require('../models/follows/followBeauty');
var FollowTalent = require('../models/follows/followTalent');
var FollowSocial = require('../models/follows/followSocial');
var FanBeauty = require('../models/fans/fanBeauty');
var FanTalent = require('../models/fans/fanTalent');
var FanSocial = require('../models/fans/fanSocial');
var UserBeauty = require('../models/users/userBeauty');
var UserTalent = require('../models/users/userTalent');
var UserSocial = require('../models/users/userSocial');

exports.create = (req, res) => {
	var follow='';
	var fan='';
	if(req.body.flag == 1){
		if(req.headers.type == "vrobeauty")
		{
			FollowBeauty.findOne({'user_id':req.body.user_id}, function(err, votesB) {
				if(votesB != null){
					FollowBeauty.updateOne({'user_id':req.body.user_id},{ $push: { follow_id: req.body.follow_id}},function(err,results){
					});
					updateFollowUser(req.body.user_id,req.headers.type,1);

					FanBeauty.updateOne({'user_id':req.body.follow_id},{ $push: { follow_id: req.body.user_id}},function(err,results){
					});
					updateFanUser(req.body.follow_id,req.headers.type,1);
				}else{
					follow = new FollowBeauty(req.body);
					fan = new FanBeauty(req.body);
					follow.save((err, data) => {
						if (err) {
							return res.status(400).send({code:400,message:err.errors})
						} else {
							updateFollowUser(req.body.user_id,req.headers.type,1);
						}
					});
					fan.save((err, data) => {
						if (err) {
							return res.status(400).send({code:400,message:err.errors})
						} else {
							updateFanUser(req.body.follow_id,req.headers.type,1);
						}
					});
				}
				return res.status(200).send({code:200,message:'successfully'})
			});
		}
		else if(req.headers.type == "vrotalent")
		{
			FollowTalent.findOne({'user_id':req.body.user_id}, function(err, votesB) {
				if(votesB != null){
					FollowTalent.updateOne({'user_id':req.body.user_id},{ $push: { follow_id: req.body.follow_id}},function(err,results){
					});
					updateFollowUser(req.body.user_id,req.headers.type,1);

					FanTalent.updateOne({'user_id':req.body.follow_id},{ $push: { follow_id: req.body.user_id}},function(err,results){
					});
					updateFanUser(req.body.follow_id,req.headers.type,1);
				}else{
					follow = new FollowTalent(req.body);
					fan = new FanTalent(req.body);
					follow.save((err, data) => {
						if (err) {
							return res.status(400).send({code:400,message:err.errors})
						} else {
							updateFollowUser(req.body.user_id,req.headers.type,1);
						}
					});
					fan.save((err, data) => {
						if (err) {
							return res.status(400).send({code:400,message:err.errors})
						} else {
							updateFanUser(req.body.follow_id,req.headers.type,1);
						}
					});
				}
				return res.status(200).send({code:200,message:'successfully'})
			});
		}
		else 
		{
			FollowSocial.findOne({'user_id':req.body.user_id}, function(err, votesB) {
				if(votesB != null){
					FollowSocial.updateOne({'user_id':req.body.user_id},{ $push: { follow_id: req.body.follow_id}},function(err,results){
					});
					updateFollowUser(req.body.user_id,req.headers.type,1);

					FanSocial.updateOne({'user_id':req.body.follow_id},{ $push: { follow_id: req.body.user_id}},function(err,results){
					});
					updateFanUser(req.body.follow_id,req.headers.type,1);
				}else{
					follow = new FollowSocial(req.body);
					fan = new FanSocial(req.body);
					follow.save((err, data) => {
						if (err) {
							return res.status(400).send({code:400,message:err.errors})
						} else {
							updateFollowUser(req.body.user_id,req.headers.type,1);
						}
					});
					fan.save((err, data) => {
						if (err) {
							return res.status(400).send({code:400,message:err.errors})
						} else {
							updateFanUser(req.body.follow_id,req.headers.type,1);
						}
					});
				}
				return res.status(200).send({code:200,message:'successfully'})
			});
		}
	}else{

		if(req.headers.type == "vrobeauty")
		{
			FollowBeauty.updateOne({'user_id':req.body.user_id},{ $pull: { follow_id: req.body.follow_id}},function(err,results){
			});
			updateFollowUser(req.body.user_id,req.headers.type,-1);

			FanBeauty.updateOne({'user_id':req.body.follow_id},{ $pull: { follow_id: req.body.user_id}},function(err,results){
			});
			updateFanUser(req.body.follow_id,req.headers.type,-1);
			return res.status(200).send({code:200,message:'updated successfully'})
		}else if(req.headers.type == "vrotalent"){
			
			FollowTalent.updateOne({'user_id':req.body.user_id},{ $pull: { follow_id: req.body.follow_id}},function(err,results){
			});
			updateFollowUser(req.body.user_id,req.headers.type,-1);

			FanTalent.updateOne({'user_id':req.body.follow_id},{ $pull: { follow_id: req.body.user_id}},function(err,results){
			});
			updateFanUser(req.body.follow_id,req.headers.type,-1);
			return res.status(200).send({code:200,message:'updated successfully'})
		}
		else
		{
			FollowSocial.updateOne({'user_id':req.body.user_id},{ $pull: { follow_id: req.body.follow_id}},function(err,results){
			});
			updateFollowUser(req.body.user_id,req.headers.type,-1);

			FanSocial.updateOne({'user_id':req.body.follow_id},{ $pull: { follow_id: req.body.user_id}},function(err,results){
			});
			updateFanUser(req.body.follow_id,req.headers.type,-1);
			return res.status(200).send({code:200,message:'updated successfully'})
		}
	}

 //    		var post='';
	// if(req.headers.type == "vrobeauty")
	// 	post = new FollowBeauty(req.body);
	// else if(req.headers.type == "vrotalent")		
	// 	post = new FollowTalent(req.body);
	// else if(req.headers.type == "vrosocial")
	// 	post = new FollowSocial(req.body);
	// else
	// 	return res.status(400).send({code:400,message:"Type is not acceptable"})

	// post.save((err, user) => {
	// 	if (err) {
	// 		return res.status(400).send({code:400,message:err.errors})
	// 	} else {
	// 		return res.status(200).send({code:200,message:'Posted successfully',data:user})
	// 	}
	// });


}

exports.lists = (req, res) => {
	/*sort based on favorites*/
	/*sort based on follows*/
	/*limit and skip*/
	/**/
	FollowBeauty.find({user_id:req.query.user_id}, function(err, posts) {
		if (err) return res.status(401).send({code:401,error:err});
		return res.status(200).send({code:200,data:posts})
	});		
}

/*list all users*/
// exports.listAll = function(req, res) {
// 	switch(req.headers.type ){
// 		case "vrobeauty":
// 		PostBeauty.find({}, function(err, posts) {
// 			if (err) return res.status(401).send({code:401,error:err});
// 			return res.status(200).send({code:200,data:posts})
// 		});		
// 		break;
// 		case "vrotalent":
// 		PostTalent.find({}, function(err, posts) {
// 			if (err) return res.status(401).send({code:401,error:err});
// 			return res.status(200).send({code:200,data:posts})
// 		});
// 		break;
// 		case "vrosocial":
// 		PostSocial.find({}, function(err, posts) {
// 			if (err) return res.status(401).send({code:401,error:err});
// 			return res.status(200).send({code:200,data:posts})
// 		});
// 		break;
// 		default: return res.status(400).send({code:400,message:"Type is not acceptable"})
// 	}
// };

exports.delete = function(req, res, next) {
	switch(req.headers.type ){
		case "vrobeauty":
		FollowBeauty.findOneAndDelete({
			_id: req.params.id
		},req.body, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,message:"Deleted successfully"});
		});
		break;
		case "vrotalent":
		FollowTalent.findOneAndDelete({
			_id: req.params.id
		},req.body, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,message:"Deleted successfully"});
		});
		break;
		case "vrosocial":
		FollowSocial.findOneAndDelete({
			_id: req.params.id
		},req.body, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,message:"Deleted successfully"});
		});
		break;
		default: return res.status(400).send({code:400,message:"Type is not acceptable"})
	}
};


function updateFollowUser(user_id,type,val){
	switch(type){
		case "vrobeauty":
		UserBeauty.findByIdAndUpdate({_id: user_id}, {$inc:{follows:val}},function(err, raw) {
		});
		break;
		case "vrotalent":
		UserTalent.findByIdAndUpdate({_id: user_id},{$inc:{follows:val}},function(err, raw) {
		});
		break;
		case "vrosocial":
		UserSocial.findByIdAndUpdate({_id: user_id},{$inc:{follows:val}},function(err, raw) {
		});
		break;
		default: return false;
		break;
	}
};

function updateFanUser(user_id,type,val){
	switch(type){
		case "vrobeauty":
		UserBeauty.findByIdAndUpdate({_id: user_id}, {$inc:{fans:val}},function(err, raw) {
		});
		break;
		case "vrotalent":
		UserTalent.findByIdAndUpdate({_id: user_id},{$inc:{fans:val}},function(err, raw) {
		});
		break;
		case "vrosocial":
		UserSocial.findByIdAndUpdate({_id: user_id},{$inc:{fans:val}},function(err, raw) {
		});
		break;
		default: return false;
		break;
	}
};
