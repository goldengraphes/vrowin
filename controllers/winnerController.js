
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var WinnerBeauty = require('../models/winners/winnerBeauty');
var WinnerTalent = require('../models/winners/winnerTalent');

exports.create = (req, res) => {
	return res.status(200).send({code:200,message:"development in progress"})
}

exports.lists = (req, res) => {	
	switch(req.headers.type ){
		case "vrobeauty":
		WinnerBeauty.find({}, function(err, posts) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,data:posts})
		});	
		break;
		case "vrotalent":
		WinnerTalent.find({}, function(err, posts) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,data:posts})
		});	
		break;
		default: return res.status(400).send({code:400,message:"Type is not acceptable"})
	}
}

exports.show = function(req, res) {
	switch(req.headers.type ){
		case "vrobeauty":
		WinnerBeauty.findOne({
			_id: req.params.id
		}, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,data:user});
		});
		break;
		case "vrotalent":
		WinnerTalent.findOne({
			_id: req.params.id
		}, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,data:user});
		});
		break;
		default: return res.status(400).send({code:400,message:"Type is not acceptable"})
	}
};

exports.update = function(req, res) {
	switch(req.headers.type ){
		case "vrobeauty":
		WinnerBeauty.findByIdAndUpdate({
			_id: req.params.id
		},req.body, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,message:"Successfully updated"});
		});
		break;
		case "vrotalent":
		WinnerTalent.findByIdAndUpdate({
			_id: req.params.id
		},req.body, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,message:"Successfully updated"});
		});
		break;
		default: return res.status(400).send({code:400,message:"Type is not acceptable"})
	}
};


//pending -  remove season also 
exports.delete = function(req, res, next) {
	switch(req.headers.type ){
		case "vrobeauty":
		WinnerBeauty.findOneAndDelete({
			_id: req.params.id
		},req.body, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,message:"Deleted successfully"});
		});
		break;
		case "vrotalent":
		WinnerTalent.findOneAndDelete({
			_id: req.params.id
		},req.body, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,message:"Deleted successfully"});
		});
		break;
		default: return res.status(400).send({code:400,message:"Type is not acceptable"})
	}
};

