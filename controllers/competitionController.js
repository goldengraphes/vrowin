
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var CompetitionBeauty = require('../models/competitions/competitionBeauty');
var CompetitionTalent = require('../models/competitions/competitionTalent');

exports.create = (req, res) => {
	var post='';
	if(req.headers.type == "vrobeauty")
		post = new CompetitionBeauty(req.body);
	else if(req.headers.type == "vrotalent")
		post = new CompetitionTalent(req.body);
	else
		return res.status(400).send({code:400,message:"Type is not acceptable"})

	post.save((err, user) => {
		if (err) {
			return res.status(400).send({code:400,message:err.errors})
		} else {
			return res.status(200).send({code:200,message:'Competion created successfully',data:user})
		}
	});
}

exports.lists = (req, res) => {	
	switch(req.headers.type ){
		case "vrobeauty":
		CompetitionBeauty.find({}, function(err, posts) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,data:posts})
		});	
		break;
		case "vrotalent":
		CompetitionTalent.find({}, function(err, posts) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,data:posts})
		});	
		break;
		default: return res.status(400).send({code:400,message:"Type is not acceptable"})
	}
}

exports.show = function(req, res) {
	switch(req.headers.type ){
		case "vrobeauty":
		CompetitionBeauty.findOne({
			_id: req.params.id
		}, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,data:user});
		});
		break;
		case "vrotalent":
		CompetitionTalent.findOne({
			_id: req.params.id
		}, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,data:user});
		});
		break;
		default: return res.status(400).send({code:400,message:"Type is not acceptable"})
	}
};

exports.update = function(req, res) {
	switch(req.headers.type ){
		case "vrobeauty":
		CompetitionBeauty.findByIdAndUpdate({
			_id: req.params.id
		},req.body, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,message:"Successfully updated"});
		});
		break;
		case "vrotalent":
		CompetitionTalent.findByIdAndUpdate({
			_id: req.params.id
		},req.body, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,message:"Successfully updated"});
		});
		break;
		default: return res.status(400).send({code:400,message:"Type is not acceptable"})
	}
};


//pending -  remove season also 
exports.delete = function(req, res, next) {
	switch(req.headers.type ){
		case "vrobeauty":
		CompetitionBeauty.findOneAndDelete({
			_id: req.params.id
		},req.body, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,message:"Deleted successfully"});
		});
		break;
		case "vrotalent":
		CompetitionTalent.findOneAndDelete({
			_id: req.params.id
		},req.body, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,message:"Deleted successfully"});
		});
		break;
		default: return res.status(400).send({code:400,message:"Type is not acceptable"})
	}
};

