
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var SeasonsBeauty = require('../models/seasons/seasonBeauty');
var SeasonsTalent = require('../models/seasons/seasonTalent');

exports.create = (req, res) => {
	var post='';
	if(req.headers.type == "vrobeauty")
		post = new SeasonsBeauty(req.body);
	else if(req.headers.type == "vrotalent")
		post = new SeasonsTalent(req.body);
	else
		return res.status(400).send({code:400,message:"Type is not acceptable"})

	post.save((err, user) => {
		if (err) {
			return res.status(400).send({code:400,message:err.errors})
		} else {
			return res.status(200).send({code:200,message:'Competion created successfully',data:user})
		}
	});
}

exports.lists = (req, res) => {	
	switch(req.headers.type ){
		case "vrobeauty":
		SeasonsBeauty.find({}, function(err, posts) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,data:posts})
		});	
		break;
		case "vrotalent":
		SeasonsTalent.find({}, function(err, posts) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,data:posts})
		});	
		break;
		default: return res.status(400).send({code:400,message:"Type is not acceptable"})
	}
}

exports.show = function(req, res) {
	switch(req.headers.type ){
		case "vrobeauty":
		SeasonsBeauty.findOne({
			_id: req.params.id
		}, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,data:user});
		}).populate("competition");
		break;
		case "vrotalent":
		SeasonsTalent.findOne({
			_id: req.params.id
		}, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,data:user});
		});
		break;
		default: return res.status(400).send({code:400,message:"Type is not acceptable"})
	}
};

exports.update = function(req, res) {
	switch(req.headers.type ){
		case "vrobeauty":
		SeasonsBeauty.findByIdAndUpdate({
			_id: req.params.id
		},req.body, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,message:"Successfully updated"});
		});
		break;
		case "vrotalent":
		SeasonsTalent.findByIdAndUpdate({
			_id: req.params.id
		},req.body, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,message:"Successfully updated"});
		});
		break;
		default: return res.status(400).send({code:400,message:"Type is not acceptable"})
	}
};


//pending -  remove season also 
exports.delete = function(req, res, next) {
	switch(req.headers.type ){
		case "vrobeauty":
		SeasonsBeauty.findOneAndDelete({
			_id: req.params.id
		},req.body, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,message:"Deleted successfully"});
		});
		break;
		case "vrotalent":
		SeasonsTalent.findOneAndDelete({
			_id: req.params.id
		},req.body, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,message:"Deleted successfully"});
		});
		break;
		default: return res.status(400).send({code:400,message:"Type is not acceptable"})
	}
};

