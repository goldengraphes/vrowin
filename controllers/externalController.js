var moment = require('moment');
var CompetitionBeauty = require('../models/competitions/competitionBeauty');
var CompetitionTalent = require('../models/competitions/competitionTalent');
var SeasonBeauty = require('../models/seasons/seasonBeauty');
var SeasonTalent = require('../models/seasons/seasonTalent');
var WaitingCompetitionBeauty = require('../models/waitingcompetitions/waitingcompetitionBeauty');
var WaitingCompetitionTalent = require('../models/waitingcompetitions/waitingcompetitionTalent');
var OnCompetitionBeauty = require('../models/oncompetitions/oncompetitionBeauty');
var OnCompetitionTalent = require('../models/oncompetitions/oncompetitionTalent');
var ArchivedCompetitionBeauty = require('../models/archivedcompetitions/archivedcompetitionBeauty');
var ArchivedCompetitionTalent = require('../models/archivedcompetitions/archivedcompetitionTalent');
var RoundBeauty = require('../models/rounds/roundBeauty');
var RoundTalent = require('../models/rounds/roundTalent');
/*
* select list form the waiting queue
* select the today opening competitions
* remove competions id from waiting queue and add into on competition queue
* create round and update round id in seasons 
*/
exports.startCompetions = (req, res) => {

	if(type == "vrobeauty"){
		WaitingCompetitionBeauty.find({}).populate({
			path: 'competition',
			populate: { 
				path: 'waiting_season'
			}
		}
		).exec((err, competition)=>{
			if(err) return false;
			competition.map((val,index) => {
				if(val.competition != null && val.competition != ''){
					var waitingLists = val.competition.waiting_season;
					waitingLists.map((value,ind)=>{

						if (moment(value.startOn).format('l') <= moment().format('l'))
						{
							WaitingCompetitionBeauty.findOneAndDelete({competition: value.competition},null, function(err, user) {
							});
							CompetitionBeauty.updateOne({_id:value.competition},{ $pull: { waiting_season:value._id},active_season:value._id },function(err,results){
							});
							onCompetitions(value.competition,'vrobeauty');
							createRound(value._id, 'vrobeauty')
						}
					}); 
				}
			});
			return res.status(200).send({code:200,message:'success'});
		});
	} else {
		WaitingCompetitionTalent.find({}).populate({
			path: 'competition',
			populate: { 
				path: 'waiting_season'
			}
		}
		).exec((err, competition)=>{
			if(err) return false;
			competition.map((val,index) => {
				if(val.competition != null && val.competition != ''){
					var waitingLists = val.competition.waiting_season;
					waitingLists.map((value,ind)=>{
						if (moment(value.startOn).format('l') <= moment().format('l'))
						{
							WaitingCompetitionTalent.findOneAndDelete({competition: value.competition},null, function(err, user) {
							});
							CompetitionTalent.updateOne({_id:value.competition},{ $pull: { waiting_season:value._id},active_season:value._id },function(err,results){
							});
							onCompetitions(value.competition,'vrotalent');
							createRound(value._id, 'vrotalent')
						}
					}); 
				}
			});
			return res.status(200).send({code:200,message:'success'});
		});
	}
}

/*Create On Going competions with id reference*/
function onCompetitions(competition_id, type){
	var competition='';
	if(type == "vrobeauty")
		competition = new OnCompetitionBeauty({competition:competition_id});
	else if(type == "vrotalent")
		competition = new OnCompetitionTalent({competition:competition_id});
	competition.save((err, user) => {
	});
}

/* Create New round*/
function createRound(season_id, type){
	var round ='';
	if(type == "vrobeauty")
		round = new RoundBeauty();
	else if(type == "vrotalent")
		round = new RoundTalent();
	round.save((err, result) => {
		if(type == "vrobeauty") {
			SeasonBeauty.updateOne({_id:season_id},{active_round:result._id},function(err,results){
			});
		}
		else { 
			SeasonTalent.updateOne({_id:season_id},{active_round:result._id},function(err,results){
			});
		}
	});
}

/* 
* Get upcoming competitions
* Get on competitions
* Get upcoming competitions
*/
exports.getCompetitions = function(req, res) {
	if(req.headers.type == "vrobeauty"){
		if(req.query.searchType == 0){
			getWaitingCompetitionBeauty('vrobeauty',function(results){
				return res.status(200).send({code:200,data:results})
			})
		}else if(req.query.searchType == 1){
			getOnCompetitionBeauty('vrobeauty',function(results){
				return res.status(200).send({code:200,data:results})
			})
		}else if(req.query.searchType == 2){
			getArchivedCompetitionBeauty('vrobeauty',function(results){
				return res.status(200).send({code:200,data:results})
			})
		}else{
			return res.status(400).send({code:400,message:""})
		}
	}else{
		if(req.query.searchType == 0){
			getWaitingCompetitionTalent('vrotalent',function(results){
				return res.status(200).send({code:200,data:results})
			})
		}else if(req.query.searchType == 1){
			getOnCompetitionTalent('vrotalent',function(results){
				return res.status(200).send({code:200,data:results})
			})
		}else if(req.query.searchType == 2){
			getArchivedCompetitionTalent('vrotalent',function(results){
				return res.status(200).send({code:200,data:results})
			})
		}else{
			return res.status(400).send({code:400,message:""})
		}
	}
}	

function getWaitingCompetitionBeauty(type,cp){
	WaitingCompetitionBeauty.find({}).populate({
		path: 'competition',
		populate: { 
			path: 'waiting_season'
		}
	}
	).exec((err, competition)=>{er =
		cp(competition);
	});	
}

function getOnCompetitionBeauty(type,cp){
	OnCompetitionBeauty.find({}).populate({
		path: 'competition',
		populate: { 
			path: 'active_season'
		}
	}).populate({
		path: 'competition',
		populate: { 
			path: 'archived_season'
		}
	}
	).exec((err, competition)=>{
		cp(competition);
	});	
}
function getArchivedCompetitionBeauty(type,cp){
	ArchivedCompetitionBeauty.find({}).populate({
		path: 'competition',
		populate: { 
			path: 'archived_season'
		}
	}
	).exec((err, competition)=>{
		cp(competition);
	});	
}
function getWaitingCompetitionTalent(type,cp){
	WaitingCompetitionTalent.find({}).populate({
		path: 'competition',
		populate: { 
			path: 'waiting_season'
		}
	}
	).exec((err, competition)=>{
		cp(competition);
	});	
}
function getOnCompetitionTalent(type,cp){
	OnCompetitionTalent.find({}).populate({
		path: 'competition',
		populate: { 
			path: 'active_season'
		},
		populate: { 
			path: 'archived_season'
		}
	}
	).exec((err, competition)=>{
		cp(competition);
	});	
}

function getArchivedCompetitionTalent(type,cp){
	ArchivedCompetitionTalent.find({}).populate({
		path: 'competition',
		populate: { 
			path: 'archived_season'
		}
	}
	).exec((err, competition)=>{
		cp(competition);
	});	
}

/* 
* Get season rounds
* @request seasonid
*/
exports.getSeasonRounds = function(req, res) {
	if(req.headers.type == "vrobeauty"){
		if(req.query.seasonid){
			getSeasonRoundsBeauty(req.query.seasonid,function(results){
				return res.status(200).send({code:200,data:results})
			})
		}else{
			return res.status(400).send({code:400,message:"Invalid request"})
		}
	}else{
		if(req.query.seasonid){
			getSeasonRoundsTalent(req.query.seasonid,function(results){
				return res.status(200).send({code:200,data:results})
			})
		}else{
			return res.status(400).send({code:400,message:"Invalid request"})
		}
	}
}
function getSeasonRoundsBeauty(season_id,cp){
	SeasonBeauty.findOne({_id:season_id}).populate('active_round').populate('archived_rounds').exec((err, rounds)=>{
		cp(rounds);
	});	
}
function getSeasonRoundsTalent(season_id,cp){
	SeasonTalent.findOne({_id:season_id}).populate('active_round').populate('archived_rounds').exec((err, rounds)=>{
		cp(rounds);
	});	
}

/* 
* Get round details
* @request roundid
*/
exports.getRoundDetail = function(req, res) {
	if(req.headers.type == "vrobeauty"){
		if(req.query.roundid){
			getRoundDetailBeauty(req.query.roundid,function(results){
				return res.status(200).send({code:200,data:results})
			})
		}else{
			return res.status(400).send({code:400,message:"Invalid request"})
		}
	}else{
		if(req.query.roundid){
			getRoundDetailTalent(req.query.roundid,function(results){
				return res.status(200).send({code:200,data:results})
			})
		}else{
			return res.status(400).send({code:400,message:"Invalid request"})
		}
	}
}

function getRoundDetailBeauty(season_id,cp){
	RoundBeauty.findOne({_id:season_id}).populate('posts').populate('winnerposts').exec((err, rounds)=>{
		cp(rounds);
	});	
}
function getRoundDetailTalent(season_id,cp){
	RoundTalent.findOne({_id:season_id}).populate('posts').populate('winnerposts').exec((err, rounds)=>{
		cp(rounds);
	});	
}

/*
* JOIN NOW
* post_url
* post_image
* post_type:0,1
* hash tage
* descrition
* roundid
* 
*/










