
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var VoteBeauty = require('../models/votes/voteBeauty');
var VoteTalent = require('../models/votes/voteTalent');

var PostBeauty = require('../models/posts/postBeauty');
var PostTalent = require('../models/posts/postTalent');

exports.create = (req, res) => {
	var post='';
    //set vote
    if(req.body.flag == 1){
    	if(req.headers.type == "vrobeauty")
    	{
    		post = new VoteBeauty(req.body);
    		VoteBeauty.findOne({'user_id':req.body.user_id}, function(err, votesB) {
    			if(votesB != null){
    				VoteBeauty.updateOne({'user_id':req.body.user_id},{ $push: { post_id: req.body.post_id}},function(err,results){
    				});
    				updateVotePost(req.body.post_id,req.headers.type,1);
    			}else{
    				post.save((err, vote) => {
    					if (err) {
    						return res.status(400).send({code:400,message:err.errors})
    					} else {
    						updateVotePost(req.body.post_id,req.headers.type,1);
    					}
    				});
    			}
    			return res.status(200).send({code:200,message:'Posted successfully'})
    		});
    	}else if(req.headers.type == "vrotalent"){
    		post = new VoteTalent(req.body);
    		VoteTalent.findOne({'user_id':req.body.user_id}, function(err, votesB) {
    			if(votesB != null){
    				VoteTalent.updateOne({'user_id':req.body.user_id},{ $push: { post_id: req.body.post_id}},function(err,results){
    				});
    				updateVotePost(req.body.post_id,req.headers.type,1);
    			}else{
    				post.save((err, vote) => {
    					if (err) {
    						return res.status(400).send({code:400,message:err.errors})
    					} else {
    						updateVotePost(req.body.post_id,req.headers.type,1);
    					}
    				});
    			}
    			return res.status(200).send({code:200,message:'Posted successfully'})
    		});
    	}
    	else
    		return res.status(400).send({code:400,message:"Type is not acceptable"});

    }else{
		//unset vote
		if(req.headers.type == "vrobeauty")
		{
			VoteBeauty.updateOne({'user_id':req.body.user_id},{ $pull: { post_id: req.body.post_id}},function(err,results){
			});
			updateVotePost(req.body.post_id, req.headers.type, -1);
			return res.status(200).send({code:200,message:'Posted successfully'})
		}else if(req.headers.type == "vrotalent"){
			
			VoteTalent.updateOne({'user_id':req.body.user_id},{ $pull: { post_id: req.body.post_id}},function(err,results){
			});
			updateVotePost(req.body.post_id, req.headers.type, -1);
			return res.status(200).send({code:200,message:'Posted successfully'})
		}
		else
			return res.status(400).send({code:400,message:"Type is not acceptable"});
	}
}

exports.lists = (req, res) => {
	switch(req.headers.type ){
		case "vrobeauty":
		VoteBeauty.findOne({user_id:req.query.user_id}).exec(function(err, votes) {
			if (err) return res.status(401).send({code:401,error:err});
			if(votes)
				return res.status(200).send({code:200,data:votes.post_id});
			return res.status(200).send({code:200,data:[]});
		});		
		break;
		case "vrotalent":
		VoteTalent.findOne({user_id:req.query.user_id}).exec(function(err, votes) {
			if (err) return res.status(401).send({code:401,error:err});
			if(votes)
				return res.status(200).send({code:200,data:votes.post_id});
			return res.status(200).send({code:200,data:[]});
		});
		break;
		default: return res.status(400).send({code:400,message:"Type is not acceptable"})
	}
}

exports.delete = function(req, res, next) {
	switch(req.headers.type ){
		case "vrobeauty":
		VoteBeauty.findOneAndDelete({
			_id: req.params.id
		},req.body, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,message:"Deleted successfully"});
		});
		break;
		case "vrotalent":
		VoteTalent.findOneAndDelete({
			_id: req.params.id
		},req.body, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,message:"Deleted successfully"});
		});
		break;
		default: return res.status(400).send({code:400,message:"Type is not acceptable"})
	}
};

function updateVotePost(post_id,type,val){
	switch(type){
		case "vrobeauty":
		PostBeauty.findByIdAndUpdate({_id:post_id}, {$inc:{votes:val}},function(err, raw) {
		});
		break;
		case "vrotalent":
		PostTalent.findByIdAndUpdate({_id: post_id},{$inc:{votes:val}},function(err, raw) {
		});
		break;
		default: return false;
		break;
	}
};

