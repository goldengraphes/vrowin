
var jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt');
var FavouritBeauty = require('../models/favourits/favouritBeauty');
var FavouritTalent = require('../models/favourits/favouritTalent');
var FavouritSocial = require('../models/favourits/favouritSocial');

var PostBeauty = require('../models/posts/postBeauty');
var PostTalent = require('../models/posts/postTalent');
var PostSocial = require('../models/posts/postSocial');


exports.create = (req, res) => {
	var favourite='';
    //set vote
    if(req.body.flag == 1){
    	if(req.headers.type == "vrobeauty")
    	{
    		favourite = new FavouritBeauty(req.body);
    		FavouritBeauty.findOne({'user_id':req.body.user_id}, function(err, votesB) {
    			if(votesB != null){
    				FavouritBeauty.updateOne({'user_id':req.body.user_id},{ $push: { post_id: req.body.post_id}},function(err,results){
    				});
    				updateFavoritPost(req.body.post_id,req.headers.type,1);
    			}else{
    				favourite.save((err, vote) => {
    					if (err) {
    						return res.status(400).send({code:400,message:err.errors})
    					} else {
    						updateFavoritPost(req.body.post_id,req.headers.type,1);
    					}
    				});
    			}
    			return res.status(200).send({code:200,message:'Posted successfully'})
    		});
    	}else if(req.headers.type == "vrotalent"){
    		favourite = new FavouritTalent(req.body);
    		FavouritTalent.findOne({'user_id':req.body.user_id}, function(err, votesB) {
    			if(votesB != null){
    				FavouritTalent.updateOne({'user_id':req.body.user_id},{ $push: { post_id: req.body.post_id}},function(err,results){
    				});
    				updateFavoritPost(req.body.post_id,req.headers.type,1);
    			}else{
    				favourite.save((err, vote) => {
    					if (err) {
    						return res.status(400).send({code:400,message:err.errors})
    					} else {
    						updateFavoritPost(req.body.post_id,req.headers.type,1);
    					}
    				});
    			}
    			return res.status(200).send({code:200,message:'Posted successfully'})
    		});
    	}
    	else{
    		favourite = new FavouritSocial(req.body);
    		FavouritSocial.findOne({'user_id':req.body.user_id}, function(err, votesB) {
    			if(votesB != null){
    				FavouritSocial.updateOne({'user_id':req.body.user_id},{ $push: { post_id: req.body.post_id}},function(err,results){
    				});
    				updateFavoritPost(req.body.post_id,req.headers.type,1);
    			}else{
    				favourite.save((err, vote) => {
    					if (err) {
    						return res.status(400).send({code:400,message:err.errors})
    					} else {
    						updateFavoritPost(req.body.post_id,req.headers.type,1);
    					}
    				});
    			}
    			return res.status(200).send({code:200,message:'Posted successfully'})
    		});
    	}

    }else{
		//unset vote
		if(req.headers.type == "vrobeauty")
		{
			FavouritBeauty.updateOne({'user_id':req.body.user_id},{ $pull: { post_id: req.body.post_id}},function(err,results){
			});
			updateFavoritPost(req.body.post_id, req.headers.type, -1);
			return res.status(200).send({code:200,message:'Posted successfully'})
		}else if(req.headers.type == "vrotalent"){
			
			FavouritTalent.updateOne({'user_id':req.body.user_id},{ $pull: { post_id: req.body.post_id}},function(err,results){
			});
			updateFavoritPost(req.body.post_id, req.headers.type, -1);
			return res.status(200).send({code:200,message:'Posted successfully'})
		}
		else
		{
			FavouritSocial.updateOne({'user_id':req.body.user_id},{ $pull: { post_id: req.body.post_id}},function(err,results){
			});
			updateFavoritPost(req.body.post_id, req.headers.type, -1);
			return res.status(200).send({code:200,message:'Posted successfully'})
		}
	}
}

/*list all users*/
exports.lists = function(req, res) {
	switch(req.headers.type ){
		case "vrobeauty":
		FavouritBeauty.findOne({user_id:req.query.user_id}, function(err, favorites) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,data:favorites.post_id})
		});			
		break;
		case "vrotalent":
		FavouritTalent.findOne({user_id:req.query.user_id}, function(err, favorites) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,data:favorites.post_id})
		});	
		break;
		case "vrosocial":
		FavouritSocial.findOne({user_id:req.query.user_id}, function(err, favorites) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,data:favorites.post_id})
		});	
		break;
		default: return res.status(400).send({code:400,message:"Type is not acceptable"})
	}
};

exports.delete = function(req, res, next) {
	switch(req.headers.type ){
		case "vrobeauty":
		FavouritBeauty.findOneAndDelete({
			_id: req.params.id
		},req.body, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,message:"Deleted successfully"});
		});
		break;
		case "vrotalent":
		FavouritTalent.findOneAndDelete({
			_id: req.params.id
		},req.body, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,message:"Deleted successfully"});
		});
		break;
		case "vrosocial":
		FavouritSocial.findOneAndDelete({
			_id: req.params.id
		},req.body, function(err, user) {
			if (err) return res.status(401).send({code:401,error:err});
			return res.status(200).send({code:200,message:"Deleted successfully"});
		});
		break;
		default: return res.status(400).send({code:400,message:"Type is not acceptable"})
	}
};

function updateFavoritPost(post_id,type,val){
	switch(type){
		case "vrobeauty":
		PostBeauty.findByIdAndUpdate({_id:post_id}, {$inc:{favourits:val}},function(err, raw) {
		});
		break;
		case "vrotalent":
		PostTalent.findByIdAndUpdate({_id: post_id},{$inc:{favourits:val}},function(err, raw) {
		});
		case "vrosocial":
		PostSocial.findByIdAndUpdate({_id: post_id},{$inc:{favourits:val}},function(err, raw) {
		});
		break;
		default: return false;
		break;
	}
};

